# Defining a Character

Defining a character should be the first step whenever you start making a game. A character is defined using a `character` block:

```nim
character alice:
  shortname = "Alice"
  fullname = "Alice Smith"
```

The code above defines a character called `alice`. The character's identifier (the `alice` part) is very important. You will use it in many places throughout your game. For example, you can make Alice say something by putting a line like this in the game's script:

```nim
alice "Hello, world!"
```

If you don't want to type out Alice's entire name every time she says something, it might be easier to define her like so:

```nim
character a:     # notice the identifier is just 'a' instead of 'alice'
  shortname = "Alice"
  fullname = "Alice Smith"
```

# shortname and fullname

In the above examples, the character's short name is Alice, and the character's full name is "Alice Smith."

`shortname` is very important! When the character speaks, their `shortname` will appear on the screen with the dialogue.

Don't worry much about `fullname` for now. Chroma doesn't use it for anything *(yet)*.

# Layers

Our definition of `alice` is perfectly valid. Chroma will compile the above samples without complaint. However, if we want to show Alice on the screen, we must define layers for Alice. The code below creates two layers for alice: one layer called `face`, and the other called `outfit`:

```nim
character alice:
  shortname = "Alice"
  fullname = "Alice Smith"
  
  layer outfit:
    uniform = "images/alice/outfit_uniform.png"
    pajamas = "images/alice/outfit_pajamas.png"
    party = "images/alice/outfit_party.png"
  
  layer face:
    happy = "images/alice/face_happy.png"
    sad = "images/alice/face_sad.png"
```

In the above example, the **layer** `outfit` has three **varaints**: `uniform`, `pajamas`, and `party`. Likewise, the layer `face` has two variants: `happy` and `sad`. Each variant points to a PNG image file. All of a character's variants must have images that are the same resolution. The file paths shown above are just examples- you don't have to follow the naming scheme they use.

When you show a character on the screen, one varaint from each layer will be shown. Putting this command in your script will make Alice appear on the screen, with the variants party and happy:

```nim
show alice, party, happy
```

You don't have to explicitly state all the variants you want, though:

```
show alice, party, happy    # will show alice with the party outfit, and happy face.
show alice, sad             # still shows the party outfit, but with the sad face.
```

The nature of the `show` command will be explored in more detail later in this guide. In the example here, every variant is just a string pointing to an image. Later examples will show how to make the image shown for a particular variant dependent on the state of the game, the time of day, or pretty much anything you can think of.
