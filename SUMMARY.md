# Summary

* [Introduction](README.md)
* [Why Chroma?](ABOUT.md)
* [Get Chroma](GET.md)

## Documentation

* [Getting Started](docs/00-sdklauncher.md)
  * [Characters](docs/01-characters.md)
  * [Backgrounds](docs/02-backgrounds.md)
  * [Script Basics](docs/03-script.md)
  * [Choice Menus](docs/12-choicemenu.md) 
  * [Labels and Jumps](docs/13-labelsjumps.md)
  * [Animations](docs/04a-animations.md)
  * [Sound and Music](docs/04b-sound.md)

* [Intermediate Features](docs/14-intermediate.md)
  * [Game Variables](docs/07-variables.md)
  * [Conditional Statements](docs/11-condlayers.md)
  * [Multi-Dialogue](docs/05-multidialogue.md)
  * [Non-Blocking Commands](docs/06-nonblocking.md)

* [Customizing the GUI](docs/15-guicustomizing.md)
  * [Fonts](docs/08-fonts.md)
  * [The UI Language](docs/20-uiintro.md)
    * Image
    * Rectangle
    * Circle
    * Vbox and Hbox
    * ImageButton
    * Sliders
    * Vscroll and Hscroll
    * TextEntry
  * [Choice Menus](docs/09-menugui.md)
  * [Dialogues](docs/10-dialoguegui.md)
  * [Options Menu](docs/16-options.md)

* [Advanced Features](docs/21-advanced.md)
  * [Custom Animations](docs/22-customshowargs.md)
  * [Internal API](docs/23-internal.md)
  * [Custom Displayables](docs/24-displayable.md)
  * [The "chroma" Macro](docs/25-macro.md)

<!--  * [Rectangle]
  * [Image]
  * [Circle]
  * [ImageButton]
  * [Sliders]
  * [Vbox and Hbox]
  * [Vscroll and Hscroll]
  * [TextEntry]-->
