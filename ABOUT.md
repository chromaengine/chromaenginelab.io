
# Why Chroma?

# Rich Feature Set

Out of the box, the default Chroma game template has:

- A straightforward main menu, where players can start and load games, and change settings.
- A comprehensive settings menu with multi-channel audio adjustment, full screen and windowed options.
- Auto-play mode, wherein dialogue advances automatically. Auto-play's timing will automatically adjust to the text being shown on the screen. Auto-play speed can be adjusted by the user.
- Fast-forward mode, allowing a player to advance quickly through material they have already seen.
- Game state rollback. Scroll back and forth in the game. Players can go back and make a different choice, too.

## Easy to Use...

Chroma games are written in a simple language, with all the nitty-gritty details managed behind the scenes. The code excerpt below is from a Chroma game:

```nim
label start:
    show bg, classroom, fadeIn(t=50)
    show alice
    alice "Good morning, Bob!"
    show bob, fadeIn(t=30)
    bob "Good morning, Alice!"
    alice italic(on), "Normally, Charlie gets here first... I wonder where he is?"
    show charlie, move(x=1000, t=50)
    charlie "Hey guys! Did I get here in time?"
```

## ...Even the Advanced Stuff.

Chroma was designed from the ground up to allow for multiple simultaneous lines of dialogue:

```nim
    alice[right] "I am saying words, and the words are showing up on the right!"
    bob[left] "Now I am saying words, and those words are showing up on the left!"
    charlie "Now I am saying words, and the words are showing up in the default place!"
```

Chroma's expressive language makes it easy to manage large amounts of artwork:

```nim
background front_of_school:
    if season == "winter": "images/schoolfront_winter.png"
    elif season == "fall": "images/schoolfront_fall.png"
    else: "images/schoolfront_spring_summer.png"
```

This "conditional image path" functionality seen above can be used with character portraits, as well.

Handling complex game states is a breeze:

```nim
    bob "Do you want to come over later? Charlie is bringing the new Super Munch game."
    if alice_confidence < 10:
        show alice, sad
        alice "I'm not sure... maybe next week."
    else:
        show alice, happy
        alice "Sure! I'd love to!"
        alice italic(on), "I can't wait to see ", love_interest
        jump visit_bob
    ...
label visit_bob:
```

# Cross Platform

Chroma is designed so that your game will work seamlessly on any recent version of Windows, Mac OS X, and Linux. Since Chroma is still in development, the primary focus is on Windows, since it is the most common choice for PC gaming.

# A Game Developer's Daydream

Chroma's macro language makes all the basics (and a lot of the not-so-basics) easy. However, it doesn't hide the inner machinery from a savvy developer's view. Chroma games can directly access the SDL2 window and renderer pointers that lie at the very core of the game's graphical backend. If you can do it with SDL2 or OpenGL, you can do it in Chroma.

Chroma is based on the [Nim programming language](https://nim-lang.org/). Weary coders fear not: if you know Python, and you have used any statically typed language, you already know enough Nim to hit the ground running with Chroma. Nim is a statically typed, compiled programming language that supports virtually any platform where C is supported. Its package manager, Nimble, has a rich assortment of popular libraries at the ready. Nim is specifically designed to produce compact binaries that are easy to distribute without any nasty runtime dependencies, and its garbage collector is designed to play nice with real-time software (e.g. *games*). Devs rejoice.

## Take Your Story Worldwide.

Chroma is designed to help your visual novel take the world by storm, and it couldn't do that without proper support for UTF-8. Chroma supports all European languages, as well as Chinese, Japanese, and Korean. If you have the fonts, we have the engine.

<div align="center">
  <video width="600" controls>
    <source src="./chinese_input.webm" type="video/webm"/>
  </video>
</div>

Chroma supports any left-to-right language without ligatures, and some with ligatures. Right-to-left support is coming soon!

## Helpful Tools

The Chroma SDK includes the Chroma base library, along with a bundle of software that can help you develop your game.

## Free and Open Source

The Chroma Visual Novel Engine is distributed under the 3-clause BSD License. Read the license [here](https://opensource.org/licenses/BSD-3-Clause). Chroma is free for use in any commercial or personal application. You will not be required to pay any royalties for using Chroma, even if your game is for-profit. Anyone is free to download Chroma's source code and modify it to suit their needs. You don't have to share your changes if you don't want to (though it would be dearly appreciated).
