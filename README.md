<!-- ![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg) -->

# The Chroma Visual Novel Engine

Chroma is a visual novel engine. It is designed to be easy to use out-of-the-box, while offering a great deal of flexibility. To an ambitious writer with no programming experience, Chroma is a simple, ergonomic framework for bringing their works to life. To a programmer, Chroma offers sophisticated event handling, straightforward management of artwork and sound effects, and excellent performance.



# Get Chroma

Chroma is in development, and will be released soon.

----

*Forked from @virtuacreative*

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
